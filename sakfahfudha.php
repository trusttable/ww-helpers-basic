<?php
// A Make.com webhook URL-je
$makeWebhook = "https://hook.eu2.make.com/39qimee56l1fru8nv22oc4ynpl7lrgdz";

// Az eredeti kérés tartalmának továbbítása
$ch = curl_init($makeWebhook);

// Kérés törzsének olvasása (pl. JSON adatok)
$data = file_get_contents('php://input');

// Beállítások a cURL-hez
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data)
]);

// Futtatás és válasz fogadása
$response = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

// Válasz visszaadása
http_response_code($httpCode);
echo $response;
?>
